@component('mail::message')
# Nieuwe review

{{ $review }}

Stars: {{ $stars }}

Door: {{ $name }}

Op: {{ $date }}

@component('mail::button', ['url' => $urlPublish, 'color' => 'green'])
Plaatsen
@endcomponent

@component('mail::button', ['url' => $urlDelete, 'color' => 'red'])
Verwijderen
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
