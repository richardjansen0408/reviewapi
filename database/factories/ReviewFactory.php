<?php

use Faker\Generator as Faker;
use App\Review;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'text' => $faker->text,
        'stars' => $faker->numberBetween(1, 5),
        'user_id' => 0,
        'published_at' => now()
            ->addDays($faker->numberBetween(1, 30) * -1)
            ->addMonths($faker->numberBetween(1, 12) * -1)
        ,
        'token' => uniqid(),
    ];
});
