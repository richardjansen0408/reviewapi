<?php

namespace App\Service;

use App\User;
use App\Review as ReviewModel;
use App\Events\ReviewWrittenEvent;

/**
 * Description of Review
 *
 * @author richard
 */
class Review
{
    /**
     * Write a new Review and fires an event
     * 
     * @param string $reviewText
     * @param int $stars
     * @param string $name
     * @return \App\Review
     */
    public function writeNewReview($reviewText, $stars, $name)
    {
        $user = $this->getReviewer($name);
        
        $review = new ReviewModel;
        $review->text  = htmlspecialchars($reviewText);
        $review->stars = $stars;
        $review->token = uniqid();
        
        $review->user()->associate($user);
        $review->save();
        
        event(new ReviewWrittenEvent($review));
        
        return $review;
    }
    
    private function getReviewer($name)
    {
         $user = User::where(['name' => $name])->first();
         
         if (!$user) {
             $user = new User;
             $user->name = $name;
             // @todo Make email 'not unique' in the database
             $user->email = str_random() . '@example.com';
             $user->password = '';
             $user->save();
         }
         
         return $user;
    }
}