<?php

namespace App\Transformers;

class ReviewTransformer extends \League\Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'user',
    ];
    
    public function transform(\App\Review $review)
    {
        return [
            'id'    => $review->id,
            'text'  => $review->text,
            'stars' => $review->stars,
            'date'  => $review->published_at->format('d-m-Y H:i:s'),
        ];
    }
    
    public function includeUser(\App\Review $review)
    {
        return $this->item($review->user, new UserTransformer);
    }
}

