<?php

use App\User;
use App\Review;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\{DB, Schema};

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
         
        DB::table('reviews')->truncate();
        DB::table('users')->truncate();
        
        factory(User::class, 20)
                ->create()
                ->each(function ($user) {
                    $review = factory(Review::class)->make();
                    $user->reviews()->save($review);
                });
    }
}
