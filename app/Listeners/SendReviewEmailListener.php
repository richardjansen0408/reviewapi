<?php

namespace App\Listeners;

use App\Events\Event;
use App\Service\Review as ReviewService;
use App\Jobs\SendReviewEmail;
use App\Events\ReviewWrittenEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReviewEmailListener implements ShouldQueue
{
    
    private $reviewService;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ReviewService $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(ReviewWrittenEvent $event)
    {
        SendReviewEmail::dispatch($event->review);
    }
}
