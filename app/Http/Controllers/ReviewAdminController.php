<?php

namespace App\Http\Controllers;

use App\Review;
use App\Service\ReviewAdmin;

class ReviewAdminController extends Controller
{
    private $service;
    
    public function __construct(ReviewAdmin $reviewAdminService)
    {
        $this->service = $reviewAdminService;
    }

    public function publish(Review $review, $token)
    {
        $this->service->publish($review, $token);
        return view('admin', ['status' => 'gepubliseerd']);
    }
    
    public function delete(Review $review, $token)
    {
        $this->service->delete($review, $token);
        return view('admin', ['status' => 'verwijderd']);
    }
}
