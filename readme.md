## Review API

Simple API om fictieve reviews te schrijven. Een bezoeker schrijft een review. 
De administrator ontvangt een e-mail om deze review goed te keuren of af te schrijven.

* Fractal Resources en Transformers
* Migrations
* Seeds
* Events
* Jobs
* Mail notification
