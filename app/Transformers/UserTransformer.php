<?php

namespace App\Transformers;

class UserTransformer extends \League\Fractal\TransformerAbstract
{
    public function transform(\App\User $user = null)
    {
        if ($user) {
            return [
                'name' => $user->name,
            ];
        }
        
        return [];
    }
}

