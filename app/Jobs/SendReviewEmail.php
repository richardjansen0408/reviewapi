<?php

namespace App\Jobs;

use App\Notifications\ReviewWrittenByCustomer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;

class SendReviewEmail
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $review;
    
     /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Review $review)
    {
        $this->review = $review;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Notification::route('mail', 'taylor@laravel.com')
            ->notify(new ReviewWrittenByCustomer($this->review));
    }
}
