<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReviewWritten extends Notification
{
    use Queueable;

    private $review;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Review $review)
    {
        $this->review = $review;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $urlPublish = '/api/reviewadmin/publish/' . $this->review->id;
        $urlDelete  = '/api/reviewadmin/delete/' . $this->review->id;
        
        return (new MailMessage)
                    ->line($this->review->text)
                    ->action('Plaatsen', url($urlPublish))
                    ->action('Verwijderen', url($urlDelete))
                ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
