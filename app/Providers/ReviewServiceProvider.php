<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ReviewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(App\Service\Review::class, function ($app) {
            return new App\Service\Review();
        });
        
        $this->app->singleton(App\Service\ReviewAdmin::class, function ($app) {
            return new App\Service\ReviewAdmin();
        });
    }
}
