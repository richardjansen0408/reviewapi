<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    
    use SoftDeletes;
    
    protected $dates = ['deleted_at', 'published_at'];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
