<?php

namespace App\Notifications;

use App\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReviewWrittenByCustomer extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /**
         * @todo Remove this ugly hack
         */
        $url = 'http://localhost/review/public/api/reviewadmin/';
        
        $urlPublish = $url . 'publish/' . $this->review->id . '/' . $this->review->token;
        $urlDelete  = $url . 'delete/' . $this->review->id . '/' . $this->review->token;
        
        $viewData = [
            'review'     => $this->review->text,
            'stars'      => $this->review->stars,
            'urlPublish' => $urlPublish,
            'urlDelete'  => $urlDelete,
            'name'       => $this->review->user->name,
            'date'       => $this->review->created_at->format('d-m-Y H:i:s'),
        ];
        
        return (new MailMessage)->markdown('mail.review.written', $viewData);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
