<?php

namespace App\Service;

use Illuminate\Support\Facades\Notification;
use App\Review;

/**
 * Description of Review
 *
 * @author richard
 */
class ReviewAdmin
{
    /**
     * Publish a Review so visitors can read it
     * @param \App\Review $review
     * @return $this
     */
    public function publish(Review $review, $token)
    {
        if ($review->token === $token) {
            $review->published_at = now();
            $review->save();
        }
        
        return $this;
    }
    
    /**
     * Soft delete a Review and it cannot be read by a visitor
     * @param Review $review
     * @return $this
     */
    public function delete(Review $review, $token)
    {
        if ($review->token === $token) {
            $review->delete();
        }
        
        return $this;
    }
}
